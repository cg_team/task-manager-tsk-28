package ru.inshakov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.entity.IHasDateStart;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByDateStart implements Comparator<IHasDateStart> {

    @NotNull
    private static final ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    @NotNull
    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasDateStart o1, @Nullable final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
