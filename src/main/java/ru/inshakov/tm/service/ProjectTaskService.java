package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.IProjectTaskService;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @Nullable final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task: tasks){
            if (projectId.equals(task.getProjectId())) tasksByProjectId.add(task);
        }
        return tasksByProjectId;
    }

    @NotNull
    @Override
    public Task bindTaskByProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskByProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Nullable
    @Override
    public Project removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllTasksByProjectId(userId, projectId);
        return projectRepository.removeOneById(userId, projectId);
    }

}

